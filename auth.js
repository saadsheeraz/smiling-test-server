const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  res.send("Hey there, Test-SERVER is up and running.");
});

module.exports = router;
