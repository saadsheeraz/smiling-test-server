const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TestFileSchema = new Schema(
  {
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    picture: {
      type: Array,
    },
    gender: {
      type: String,
    },
    dateofBirth: {
      type: String,
    },
    profession: {
      type: String,
    },

    shoeSize: {
      type: String,
    },
    hairColor: {
      type: String,
    },
    hairLength: {
      type: String,
    },
    braSize: {
      type: String,
    },

    waistSize: {
      type: String,
    },
    height: {
      type: String,
    },
    weight: {
      type: String,
    },
    castingType: {
      type: String,
    },
  },
  { collection: "TestFileSchemas" }
);
module.exports = mongoose.model("TestFileSchema", TestFileSchema);
